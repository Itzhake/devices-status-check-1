import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
//import org.apache.http.util.Asserts;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
//import sun.jvm.hotspot.utilities.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//import static org.junit.jupiter.api.Assertions.*;


public class DeviceStatusTest {


    protected static Map<String,String> qaCloud= new HashMap<String,String>();
    protected static Map<String,String> masterCloud= new HashMap<String,String>();
    protected static Map<String,String> prehistoricCloud= new HashMap<String,String>();
    protected static Map<String,String> releaseCloud= new HashMap<String,String>();

    protected int available;
    protected int error;
    protected int offline;
    protected boolean failed;


    @BeforeAll
    static void setUpAll(){
        setQaCloud();
        setMasterCloud();
        setReleaseCloud();
        setPrehistoricCloud();
    }

    @BeforeEach
    public void setUp(){
        available = 0;
        error = 0;
        offline = 0;
        failed = false;
    }

    @Test
    public void qaCloud() throws UnirestException {

        System.out.println("***************************** Start: QA Cloud ************************************");

        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.get(qaCloud.get("url")+"/api/v1/devices")
                .header("Authorization", "Bearer " + qaCloud.get("accessKey"))
                .asString();

        /**
         * Process response
         */
        JSONObject json = new JSONObject(response.getBody());
        JSONArray allDevices = null;
        try{
            allDevices = json.getJSONArray("data");
        }catch (Exception e)
        {
            Assertions.fail("No DHM contacted");
        }

        String[] devicesNamesFromConfiguration = qaCloud.get("devices").split(",");
        String[] agents = qaCloud.get("agents").split(",");

        for(int i = 0; i < allDevices.length(); i++)
        {
            JSONObject device = allDevices.getJSONObject(i);
            if(searchInList(agents, device.getString("agentName"))) {
                checkStatus(device, searchInList(devicesNamesFromConfiguration, device.getString("udid")));
            }
        }

        System.out.println("Available: " + available);
        System.out.println("Errors: " + error);
        System.out.println("Offline: " + offline);

        System.out.println("***************************** Done: QA Cloud ************************************");

    }

    @Test
    public void masterCloud() throws UnirestException {
        System.out.println("***************************** Start: Master Cloud ************************************");

        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.get(masterCloud.get("url")+"/api/v1/devices")
                .header("Authorization", "Bearer " + masterCloud.get("accessKey"))
                .asString();

        /**
         * Process response
         */
        JSONObject json = new JSONObject(response.getBody());
        JSONArray allDevices = null;
        try{
            allDevices = json.getJSONArray("data");
        }catch (Exception e)
        {
            Assertions.fail("No DHM contacted");
        }

        String[] devicesNamesFromConfiguration = masterCloud.get("devices").split(",");
        String[] agents = masterCloud.get("agents").split(",");

        for(int i = 0; i < allDevices.length(); i++)
        {
            JSONObject device = allDevices.getJSONObject(i);
            if(searchInList(agents, device.getString("agentName"))) {
                checkStatus(device, searchInList(devicesNamesFromConfiguration, device.getString("udid")));
            }
        }

        System.out.println("Available: " + available);
        System.out.println("Errors: " + error);
        System.out.println("Offline: " + offline);

        System.out.println("***************************** Done: Master Cloud ************************************");

    }

    @Test
    public void releaseCloud() throws UnirestException {
        System.out.println("***************************** Start: Release Cloud ************************************");

        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.get(releaseCloud.get("url")+"/api/v1/devices")
                .header("Authorization", "Bearer " + releaseCloud.get("accessKey"))
                .asString();

        /**
         * Process response
         */
        JSONObject json = new JSONObject(response.getBody());
        JSONArray allDevices = null;
        try{
            allDevices = json.getJSONArray("data");
        }catch (Exception e)
        {
            Assertions.fail("No DHM contacted");
        }

        String[] devicesNamesFromConfiguration = releaseCloud.get("devices").split(",");
        String[] agents = releaseCloud.get("agents").split(",");

        for(int i = 0; i < allDevices.length(); i++)
        {
            JSONObject device = allDevices.getJSONObject(i);
            if(searchInList(agents, device.getString("agentName"))) {
                checkStatus(device, searchInList(devicesNamesFromConfiguration, device.getString("udid")));
            }
        }

        System.out.println("Available: " + available);
        System.out.println("Errors: " + error);
        System.out.println("Offline: " + offline);

        System.out.println("***************************** Done: Release Cloud ************************************");

    }

    @Test
    public void prehistoricCloud() throws UnirestException {
        System.out.println("***************************** Start: Prehistoric Cloud ************************************");


        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.get(prehistoricCloud.get("url")+"/api/v1/devices")
                .header("Authorization", "Bearer " + prehistoricCloud.get("accessKey"))
                .asString();

        /**
         * Process response
         */
        JSONObject json = new JSONObject(response.getBody());
        JSONArray allDevices = null;
        try{
            allDevices = json.getJSONArray("data");
        }catch (Exception e)
        {
            Assertions.fail("No DHM contacted");
        }

        String[] devicesNamesFromConfiguration = prehistoricCloud.get("devices").split(",");
        String[] agents = prehistoricCloud.get("agents").split(",");

        for(int i = 0; i < allDevices.length(); i++)
        {
            JSONObject device = allDevices.getJSONObject(i);
            if(searchInList(agents, device.getString("agentName"))) {
                checkStatus(device, searchInList(devicesNamesFromConfiguration, device.getString("udid")));
            }
        }

        System.out.println("Available: " + available);
        System.out.println("Errors: " + error);
        System.out.println("Offline: " + offline);

        System.out.println("***************************** Done: Prehistoric Cloud ************************************");

    }

    @AfterEach
    public void finalStatus() throws Exception {
        String throwMessage = "" ;
        if(error > 0) {
            throwMessage += "There is error with: " + error + " devices. \n";
        }
//        Assertions.assertFalse((error > 0), "There is error with: " + error + " devices.");
//        System.out.println("offline: " + offline);
        if(offline > 0) {
            throwMessage += "There is: " + offline + " offline devices. \n";
        }
//        Assertions.assertFalse((offline > 0), "There is offline : " + offline + " devices.");
//        System.out.println("available: " + available);
        if((available + error) == 0){
            throwMessage += "No devices. \n";
        }
//        Assertions.assertNotEquals(0, available + error, "No devices");
        if(throwMessage.length() > 1){
            throw new Exception(throwMessage);
        }
    }

    public void checkStatus(JSONObject device, boolean inConfigurationList) {
        String currentStatus = device.getString("currentStatus");
        String statusTooltip = device.getString("statusTooltip");
        String deviceName = device.getString("deviceName");

        if(currentStatus.equals("online") || currentStatus.equals("offline")){
            if(currentStatus.equals("online")){
                if(inConfigurationList){
                    available += 1;
                }
                else {
                    System.out.println("-------------- NEW AVAILABLE DEVICE - NOT IN THE LIST -------------------");
                    System.out.println("deviceName: " + deviceName);
                    System.out.println("currentStatus: " + currentStatus);
                    System.out.println("statusTooltip: " + statusTooltip);
                    System.out.println("----------------------------------------------------");
                }
            }
            if(currentStatus.equals("offline") && inConfigurationList){
                offline += 1;
                failed = true;
                System.out.println("--------------------- OFFLINE DEVICES: -------------------");
                System.out.println("deviceName: " + deviceName);
                System.out.println("currentStatus: " + currentStatus);
                System.out.println("statusTooltip: " + statusTooltip);
                System.out.println("----------------------------------------------------");
            }
        }
        else {
            System.out.println("----------------- OTHER ERRORS: -------------------");
            System.out.println("deviceName: " + deviceName);
            System.out.println("currentStatus: " + currentStatus);
            System.out.println("statusTooltip: " + statusTooltip);
            System.out.println("----------------------------------------------------");
            error += 1;
            failed = true;
        }
    }

    public static String readConfiguration(String cloudConfigurationPath){
        String devicesSerialNumber = "";
        try {
            File myObj = new File(cloudConfigurationPath);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                devicesSerialNumber += data;
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Could not read configuration file");
            e.printStackTrace();
        }
        return devicesSerialNumber;
    }

    public static void setQaCloud(){
        qaCloud.put("Name", "QA Cloud");
        qaCloud.put("url","https://qacloud.experitest.com");
        qaCloud.put("accessKey", "eyJhbGciOiJIUzI1NiJ9.eyJ4cC51Ijo0NDA0NjQzLCJ4cC5wIjoyLCJ4cC5tIjoxNjIyNDczODAzNDcwLCJleHAiOjE5Mzc4MzM4MDQsImlzcyI6ImNvbS5leHBlcml0ZXN0In0.8Yihw39Hbqjct_-WSVNQ54LXLxkuZgQIIyWtkjeWpdo");
        qaCloud.put("devices",readConfiguration("src/test/resources/DevicesOnQaCloud.txt"));
        qaCloud.put("agents","Agent 1 - master,Agent 2 - remote");

    }

    public static void setMasterCloud(){
        masterCloud.put("Name", "Master Cloud");
        masterCloud.put("url","https://mastercloud.experitest.com");
        masterCloud.put("accessKey", "eyJhbGciOiJIUzI1NiJ9.eyJ4cC51Ijo1MDM2NTkxLCJ4cC5wIjoyLCJ4cC5tIjoxNjI0Nzg1NTc4MDI3LCJleHAiOjE5NDAxNDU1NzgsImlzcyI6ImNvbS5leHBlcml0ZXN0In0.1xCxou-wQ2ruODgMGxsjtfabLp-JGhNUTzkLEgY05hs");
        masterCloud.put("devices",readConfiguration("src/test/resources/DevicesOnMasterCloud.txt"));
        masterCloud.put("agents","Macagent 2 - 1.64 - Master,Macagent 1 - 2.12");
    }

    public static void setReleaseCloud(){
        releaseCloud.put("Name", "Release Cloud");
        releaseCloud.put("url","https://releasecloud.experitest.com");
        releaseCloud.put("accessKey", "eyJhbGciOiJIUzI1NiJ9.eyJ4cC51IjoxMTI2MTQxLCJ4cC5wIjoyLCJ4cC5tIjoxNjIzMDc4NjcwMjY4LCJleHAiOjE5Mzg0Mzg2NzAsImlzcyI6ImNvbS5leHBlcml0ZXN0In0.FtlinOZWgfQKDLyfIFB7QgL4JXS3h5Fyp35c8-sC-YI");
        releaseCloud.put("devices",readConfiguration("src/test/resources/DevicesOnReleaseCloud.txt"));
        releaseCloud.put("agents","agent");
    }

    public static void setPrehistoricCloud(){
        prehistoricCloud.put("Name", "Prehistoric Cloud");
        prehistoricCloud.put("url","https://prehistoric.experitest.com");
        prehistoricCloud.put("accessKey", "eyJhbGciOiJIUzI1NiJ9.eyJ4cC51Ijo0NiwieHAucCI6MSwieHAubSI6MTYyNTQ5NDY2MDY1NywiZXhwIjoxOTQwODU0NjYxLCJpc3MiOiJjb20uZXhwZXJpdGVzdCJ9.ym8xdhDE4P35F38wT3ZCyUCVPVqAk_ClmB9A_5u9zYc");
        prehistoricCloud.put("devices",readConfiguration("src/test/resources/DevicesOnPrehistoricCloud.txt"));
        prehistoricCloud.put("agents","prehistoric agent");
    }


    boolean searchInList(String[] strings, String searchString) {
        return Arrays.asList(strings).contains(searchString);
    }


}
